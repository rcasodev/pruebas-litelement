import { LitElement, html } from 'lit';
import '../kb-task/kb-task.js';
import * as _const from '../constants/constants.js';

class KbColumn extends LitElement {

    static get properties() {
        return {
            tasksList: { type: Array },
            columnId: { type: String },
            showTaskList: { type: Boolean },
            APIResponseEdit: { type: String }
        };
    }

    constructor() {
        super();
        this.tasksList = [];
        this.columnId = "";
        this.columnPosition = "";
        this.showTaskList = true;

    }

    render() {
            return html `
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <div class="container border border-info border-2 shadow">
                <p class="text-center fw-bold">${this.columnId}</p>
                <button class="btn btn-success" @click="${() => this.addTask({
                                                                            taskId: "tempId",
                                                                            taskName: "",
                                                                            color: "#EFEFEF",
                                                                            description: "",
                                                                            columnId: this.columnId,
                                                                            position: this.tasksList.length,
                                                                            showSave: "true",
                                                                            showEdit: "false",                                                                            
                                                                            showDelete: "true" 
                               
                                                                            })}"><strong>+</strong></button>
                <br />
                <br />
                    ${this.tasksList.map(
                        task => html`<kb-task
                                        taskId="${task.taskId}"
                                        taskName="${task.taskName}"
                                        color="${task.color}"
                                        description="${task.description}"
                                        columnId="${task.columnId}"
                                        position="${task.position}"
                                        showSave="${task.showSave}"
                                        showEdit="${task.showEdit}"                                        
                                        showDelete="${task.showDelete}"
                                        @event-goBack="${this.eventGoBack}"
                                        @event-delete="${this.eventDelete}"
                                        @event-stored="${this.eventStored}"
                                        @event-edit="${this.eventEdit}"></kb-task><br/>`
                    )}               
            </div>
         
        `
    }

    updated(changedProperties) {
        console.log(">updated()\n   propiedades actualizadas en kb-column");
        console.log(changedProperties);
        console.log(this.tasksList);

        if (changedProperties.has("APIResponseEdit")) {
            console.log("   "+APIResponseEdit.taskId);
        }
    }

    /**
     * Añade una tarea a la lista de tareas de esta columna
     * @param {*} object 
     */
    addTask(object){
        console.log(">addTask()");

        if(!this._isTempTaskInColumn()) {
            this.tasksList = [...this.tasksList,object];
        }

        console.log(this.tasksList);
        console.log("<addTask()");
    }

    /**
     * Volver atrás. Sin uso.
     * @param {*} e 
     */
    eventGoBack(e) {
        console.log(">eventGoBack()");
        console.log(e.detail)
        console.log(e.detail.taskName);
    }

    /**
     * Tras recibir el evento delete.
     * Elimina las tareas de la columna.
     * @param {*} e 
     */
    eventDelete(e) {
        console.log(">eventDelete()");
        console.log("##################################")
        console.log(e.detail)
        console.log(e.detail.taskId);

        this.tasksList = this.tasksList.filter(
            task => task.taskId != e.detail.taskId
        );
        
        console.log(this.tasksList)
        
        for (var i = 0; i<this.tasksList.length; i++) {
                this.tasksList[i].position = i;
                this.sendUpdatesToDB(this.tasksList[i]);    

        }
        
        console.log(this.tasksList);
    }

    /**
     * Tras recibir el evento de guardado.
     * Añade la tarea a la columna.     * 
     * @param {*} e 
     */
    eventStored(e) {
        console.log(">eventStored()");
        console.log(e.detail)

        this.tasksList = this.tasksList.filter(
            task => task.taskId != "tempId"
        );
        this.addTask(e.detail);
        console.log(this.tasksList);
    }

    /**
     * Tras recibir el evento de edición.
     * Actualiza las tareas de la columna.
     * @param {*} e 
     */
    eventEdit(e) {
        console.log(">eventEdit()");
        console.log(e.detail)
        console.log(e.detail.taskName);
        
        for (var i = 0; i<this.tasksList.length; i++) {
            if(this.tasksList[i].taskId === e.detail.taskId) {
                this.tasksList[i] = e.detail;
            }
        }

        console.log(this.tasksList);
    }

    /**
     * Función auxiliar, valida si ya hay una tarea en la columna en edición.
     * Devuelve el resultado de la comparación.
     * @returns 
     */
    _isTempTaskInColumn(){
        return this.tasksList.find(task => task.taskId === "tempId") != undefined
    }

    /**
     * Envía la modificación de la tarea a DB
     * @param {*} taskObjet 
     */
    sendUpdatesToDB(taskObjet){
        console.log(">sendUpdatesToDB()");
        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            console.log("   httpStatusCode: [" + xhr.status + "]");
            if (xhr.status === 202) {
                console.log("   Petición completada correctamente en actualización masiva kb-colum");
                this.APIResponseEdit = JSON.parse(xhr.responseText);
                console.log(this.APIResponseEdit);
            }
        }

        let urlParsed = "http://localhost:8080/taskboard/task/" + taskObjet.taskId;
        xhr.open("PUT", urlParsed);


        console.log(taskObjet);
        console.log("   " + JSON.stringify(taskObjet));

        console.log("   antes del JSON...")
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.send(JSON.stringify(taskObjet)); //Aquí dentro van los body
    }
}

customElements.define("kb-column", KbColumn);