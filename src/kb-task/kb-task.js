import { LitElement, html } from 'lit';
import * as _const from '../constants/constants.js';

class KbTask extends LitElement {

    static get properties() {
        return {
            taskId: { type: String },
            taskName: { type: String },
            color: { type: String },
            description: { type: String },
            columnId: { type: String },
            position: { type: String },
            APIResponse: { type: String },
            APIResponseEdit: { type: String },
            APIResponseDelete: { type: String },
            showSave: { type: String },
            showEdit: { type: String },
            showDelete: { type: String }

        };
    }

    constructor() {
        super();
        this.taskId = "";
        this.taskName = "";
        this.color = "#FF22FF";
        this.description = "";
        this.columnId = "";
        this.position = "";

        this.showSave = "false";
        this.showEdit = "true";
        this.showDelete = "true";

    }

    render() {
        return html `
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <div class="container border border-primary border-2 shadow">
            <p class="fs-6">TaskId[${this.taskId}]-----Position[${this.position}]</p>
                <form>         
                    <div class="row g-2">                    
                        <div class="col">                        
                            <input type="text" class="form-control" id="taskName" value="${this.taskName}" placeholder="Task name..." required autocomplete="off" @change="${this.updateTaskName}">
                        </div>
                        <div class="col col-auto">
                            <input type="color" class="form-control form-control-color" id="color" value="${this.color}" @change="${this.updateColor}">
                        </div>
                        <span class="border-bottom"></span>
                    </div>
                    <br />
                    <div class="row g-2">
                        <div class="col">    
                            <textarea class="form-control" id="description" rows="4" .value=${this.description} placeholder="Description..." @change="${this.updateDescription}"></textarea>
                        </div>
                        <span class="border-bottom"></span>
                    </div>
                    <br />
                    <div class="d-flex justify-content-around">
                        <button id="btnDelete" @click="${_const.ENVIRONMENT === "global" ?  this.deleteTaskHttp : this.deleteTask}" class="btn btn-danger mb-2 col-3 p-1"><strong>Borrar</strong></button>
                        <button id="btnEdit" @click="${_const.ENVIRONMENT === "global" ?  this.editTaskHttp : this.editTask}" class="btn btn-primary mb-2 col-3 p-1"><strong>Editar</strong></button>
                        <button id="btnSave" @click="${this.taskName!=""?(_const.ENVIRONMENT === "global" ?  this.storeTaskHttp : this.storeTask):{}}" class="btn btn-success mb-2 col-3 p-1"><strong>Guardar</strong></button>
                    </div>
                </form>   
            </div>
        `
    }

    updated(changedProperties) {
        console.log(">updated() en kb-task");
        console.log(changedProperties);
        changedProperties.forEach(
            (oldValue, propName) => {
                console.log("   la propiedad [" + propName +
                    "] ha cambiado de valor, anterior era [" + oldValue + "]")
            }
        )
        console.log("   Valor actual taskId" + this.taskId);
        console.log("   Valor actual taskName" + this.taskName);
        console.log("   Valor actual description" + this.description);
        console.log("   Valor actual columnId" + this.columnId);
        console.log("   Valor actual position" + this.position);

        if (changedProperties.has("APIResponse")) {
            console.log("   tarea creada correctamente en kb-task dispatchEvent");
            this.taskId = this.APIResponse.taskId;
            this.dispatchEvent(
                new CustomEvent(
                    "event-stored", {
                        detail: {
                            taskId: this.taskId,
                            taskName: this.taskName,
                            color: this.color,
                            description: this.description,
                            columnId: this.columnId,
                            position: this.position
                        }
                    }
                )
            )
        }
        if (changedProperties.has("APIResponseEdit")) {
            console.log("   tarea editada correctamente en kb-task dispatchEvent");
            this.dispatchEvent(
                new CustomEvent(
                    "event-edit", {
                        detail: {
                            taskId: this.taskId,
                            taskName: this.taskName,
                            color: this.color,
                            description: this.description,
                            columnId: this.columnId,
                            position: this.position
                        }
                    }
                )
            )
        }

        if (changedProperties.has("APIResponseDelete")) {
            console.log("   tarea borrada correctamente en kb-task dispatchEvent");
            this.dispatchEvent(
                new CustomEvent(
                    "event-delete", {
                        detail: {
                            taskId: this.taskId
                        }
                    }
                )
            )
        }


        if (changedProperties.has("showSaveDesactivado")) {
            console.log("   showSave editado con valor[" + this.showSave + "]");
            if (this.showSave === "true") {
                this.shadowRoot.getElementById("btnSave").classList.remove("d-none");
            } else {
                this.shadowRoot.getElementById("btnSave").classList.add("d-none");
            }
        }
        if (changedProperties.has("showEditDesactivado")) {
            console.log("   showEdit editado con valor[" + this.showEdit + "]");
            if (this.showEdit === "true") {
                this.shadowRoot.getElementById("btnEdit").classList.remove("d-none");
            } else {
                this.shadowRoot.getElementById("btnEdit").classList.add("d-none");
            }
        }
        if (changedProperties.has("showDeleteDesactivado")) {
            console.log("   showDelete editado con valor[" + this.showDelete + "]");
            if (this.showDelete === "true") {
                this.shadowRoot.getElementById("btnDelete").classList.remove("d-none");
            } else {
                this.shadowRoot.getElementById("btnDelete").classList.add("d-none");
            }
        }



    }

    /**
     * Actualiza el nombre de la tarea.
     * @param {*} e 
     */
    updateTaskName(e) {
        console.log(">updatedTaskName()");
        console.log("   actualizando la propiedad taskName con el valor [" + e.target.value + "]");
        this.taskName = e.target.value;
    }

    /**
     * Actualiza el color de la tarea.
     * @param {*} e 
     */
    updateColor(e) {
        console.log(">updateColor()");
        console.log("   actualizando la propiedad color con el valor [" + e.target.value + "]");
        this.color = e.target.value;
    }

    /**
     * Actualiza la descripción de la tarea.
     * @param {*} e 
     */
    updateDescription(e) {
        console.log(">updatedDescription()");
        console.log("   actualizando la propiedad description con el valor [" + e.target.value + "]");
        this.description = e.target.value;
    }

    /**
     * Guarda la tarea en DB.
     * @param {*} e 
     */
    storeTaskHttp(e) {
        console.log(">storeTaskHttp()");
        e.preventDefault();
        console.log(e);

        console.log("   Enviando el valor de la tarea a DB...");

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            if (xhr.status === 201) {
                console.log("   Petición completada correctamente en storedTaskHttp");
                this.APIResponse = JSON.parse(xhr.responseText);
                this.taskId = this.APIResponse.taskId;
                console.log(this.APIResponse);
                console.log("   Tarea modificada" + this.taskId);
            }
        }

        xhr.open("POST", "http://localhost:8080/taskboard/task");
        console.log("   antes del JSON...")

        let jsonBody = {
            "taskName": this.taskName,
            "color": this.color,
            "description": this.description,
            "columnId": this.columnId,
            "position": this.position
        }
        console.log(jsonBody);
        console.log("   " + JSON.stringify(jsonBody));

        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.send(JSON.stringify(jsonBody)); //Aquí dentro van los body

        console.log("<storeTaskHttp()");
    }

    /**
     * Para el modo local.
     * Almacena la nota. Dispara el evento.
     * @param {*} e 
     */
    storeTask(e) {
        console.log(">storeTask()");
        e.preventDefault();
        this.taskId = Date.now();
        this.dispatchEvent(
            new CustomEvent(
                "event-stored", {
                    detail: {
                        taskId: this.taskId,
                        taskName: this.taskName,
                        color: this.color,
                        description: this.description,
                        columnId: this.columnId,
                        position: this.position
                    }
                }
            )
        )
        console.log("<storeTask()");
    }

    /**
     * Borra la tarea de la DB.
     * @param {*} e 
     */
    deleteTaskHttp(e) {
        console.log(">deleteTaskHttp()");
        e.preventDefault();
        console.log(e);

        console.log("   Borrando petición en DB...");

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            if (xhr.status === 200) {
                console.log("   Petición completada correctamente en deleteTaskHttp");
                this.APIResponseDelete = this.taskId;
            }
        }

        let urlParsed = "http://localhost:8080/taskboard/task/" + this.taskId;
        xhr.open("DELETE", urlParsed);
        console.log("   antes del delete...");
        console.log(urlParsed);
        xhr.send(); //Aquí dentro van los body

        console.log("<deleteTaskHttp()");
    }

    /**
     * Para el modo local.
     * Borra la tarea en local. Dispara el evento.
     * @param {*} e 
     */
    deleteTask(e) {
        console.log(">deleteTask()");
        e.preventDefault();
        this.dispatchEvent(
            new CustomEvent(
                "event-delete", {
                    detail: {
                        taskId: this.taskId
                    }
                }
            )
        )

        console.log("<deleteTask()");
    }

    /**
     * Edita la tarea en DB.
     * @param {*} e 
     */
    editTaskHttp(e) {
        console.log(">editTaskHttp()");
        e.preventDefault();
        console.log(e);

        console.log("   Enviando el valor editado de la tarea a DB...");

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            console.log("   httpStatusCode: [" + xhr.status + "]");
            if (xhr.status === 202) {
                console.log("   Petición completada correctamente en storedTaskHttp");
                this.APIResponseEdit = JSON.parse(xhr.responseText);
                console.log(this.APIResponseEdit);
            }
        }

        let urlParsed = "http://localhost:8080/taskboard/task/" + this.taskId;
        xhr.open("PUT", urlParsed);

        let jsonBody = {
            "taskId": this.taskId,
            "taskName": this.taskName,
            "color": this.color,
            "description": this.description,
            "columnId": this.columnId,
            "position": this.position
        }
        console.log(jsonBody);
        console.log("   " + JSON.stringify(jsonBody));

        console.log("   antes del JSON...")
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.send(JSON.stringify(jsonBody)); //Aquí dentro van los body

        console.log("<editTaskHttp()");
    }

    /**
     * Para el modo local.
     * Edita la nota en local. Dispara el evento.
     * @param {*} e 
     */
    editTask(e) {
        console.log(">editTask()");
        e.preventDefault();

        this.dispatchEvent(
            new CustomEvent(
                "event-edit", {
                    detail: {
                        taskId: this.taskId,
                        taskName: this.taskName,
                        color: this.color,
                        description: this.description,
                        columnId: this.columnId,
                        position: this.position
                    }
                }
            )
        )
    }

}

customElements.define("kb-task", KbTask);