import { LitElement, html } from 'lit';
import '../kb-main-columns/kb-main-columns.js';
import * as _const from '../constants/constants.js';

class KanbanApp extends LitElement {

    static get properties() {
        return {};
    }

    constructor() {
        super();
    }

    render() {
        return html `
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <div class="container border border-secondary border-2">
                <h5>Kanban App           Version[${_const.VERSION}]        Environment[${_const.ENVIRONMENT}]</h5>
                <br />
                <kb-main-columns></kb-main-columns>
            </div>
        `
    }
}

customElements.define("kanban-app", KanbanApp);