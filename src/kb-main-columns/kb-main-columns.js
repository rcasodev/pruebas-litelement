import { LitElement, html } from 'lit';
import '../kb-column/kb-column.js';
import * as _const from '../constants/constants.js';

class KbMainColumns extends LitElement {

    static get properties() {
        return {
            columnsList: { type: Array },
            APIResponse: { type: Object }
        };
    }

    constructor() {
        super();

        this.columnsList = this.setColumnList({});
        _const.ENVIRONMENT === "global" ? this.getFullBoardHttp() : this.getFullBoard();
    }
    render() {
            return html `
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <div class="container border border-warning border-2">
            <h6>Kanban Main Columns</h6>
            <br />
            <div class="row g-2">
                ${this.columnsList.map(
                    column => html` <div class="col">
                                        <kb-column .tasksList=${column.tasksList} columnId=${column.columnId}></kb-column>
                                    </div>`                         
                )}
            </div>  
        </div>
    `
    }

    updated(changedProperties) {
        console.log(">updated() en kb-main-columns");
        console.log(changedProperties);
        changedProperties.forEach(
            (oldValue, propName) => {
                console.log("   la propiedad [" + propName +
                    "] ha cambiado de valor, anterior era [" + oldValue + "]")
            }
        );
        if(changedProperties.has("APIResponse")) {
            this.columnsList = this.setColumnList(this.APIResponse);
        }
        

    }

    /** 
    * Simula respuesta del servicio web de MongoDB
    */
    getFullBoard(){
        console.log(">getFullBoard()");
        this.APIResponse = [
            {
                "taskId": "0001",
                "taskName": "Reunion Devops1",
                "color": "#EFEFEF",
                "description": "Hoy nos reunimos con...1",
                "columnId": "TO DO",
                "position": "0"
            },
            {
                "taskId": "0002",
                "taskName": "Reunion Devops2",
                "color": "#EFEFEF",
                "description": "Hoy nos reunimos con...2",
                "columnId": "TO DO",
                "position": "1"
            },
            {
                "taskId": "0003",
                "taskName": "Reunion Devops3",
                "color": "#EFEFEF",
                "description": "Hoy nos reunimos con...3",
                "columnId": "TO DO",
                "position": "2"
            },
            {
                "taskId": "0004",
                "taskName": "Reunion Devops4",
                "color": "#EFEFEF",
                "description": "Hoy nos reunimos con...",
                "columnId": "IN PROGRESS",
                "position": "0"
            }
        ]
        console.log("<getFullBoard()");
    }

    /*
    * Petición al servicio web de MongoDB
    */
    getFullBoardHttp(){
        console.log(">getFullBoardHttp()");
        console.log("   Obteniendo datos del board...");

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            if (xhr.status === 200) {
                console.log("   Petición completada correctamente");
                this.APIResponse = JSON.parse(xhr.responseText);
                console.log(this.APIResponse);
            }
        }

        xhr.open("GET", "http://localhost:8080/taskboard/task");
        console.log("   antes del send...")
        xhr.send(); //Aquí dentro van los body
        
        console.log("<getFullBoardHttp()");
    }

    /**
     * Crea la estructura de datos del tablero en función de la respuesta json.
     * Devuelve la lista de columnas que empleará el mismo
     * @param {*} jsonResponse 
     * @returns 
     */
    setColumnList(jsonResponse) {
        console.log(">setColumnList()\n El jsonResponse es:");
        console.log(jsonResponse);

        let jsonStringfy = JSON.stringify(jsonResponse);
        console.log(jsonStringfy);
            
        let toDoColumn = {
            tasksList: this._filterColumn(jsonStringfy,"TO DO"),
            columnId: "TO DO"
        }

        let inProgressColumn = {
            tasksList: this._filterColumn(jsonStringfy,"IN PROGRESS"),
            columnId: "IN PROGRESS"
        }

        let doneColumn = {
            tasksList: this._filterColumn(jsonStringfy,"DONE"),
            columnId: "DONE"
        }

        let columnList = [toDoColumn,inProgressColumn,doneColumn];

        console.log("<setColumnList()");
        return columnList;       

    }

    /**
     * Filtra por columnId las tareas de la DB
     * Devuelve la lista de tareas del columnId filtrado
     * @param {*} jsonStringfy 
     * @param {*} columnId 
     * @returns 
     */
    _filterColumn(jsonStringfy,columnId) {
        console.log("   >_filterColumn()")
        let taskList = [];
        if(jsonStringfy != '{}') {
            taskList = JSON.parse(jsonStringfy).filter(
                function (entry) {
                    return entry.columnId === columnId;
                });
        }
        return taskList;
    }


}

customElements.define("kb-main-columns", KbMainColumns);